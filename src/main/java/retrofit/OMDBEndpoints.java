package retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OMDBEndpoints {
    @GET("/")
    Call<OMDBSearchResult> getMovieListFilteredByTitle(
            @Query("s") String searchKey, @Query("apiKey") String apiKey);

    @GET("/")
    Call<OMDBSearchResult> getMovieListFilteredByImdbId(
            @Query("i") String imdbId, @Query("apiKey") String apiKey);
}


