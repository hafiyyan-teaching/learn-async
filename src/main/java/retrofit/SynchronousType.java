package retrofit;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;

public class SynchronousType {
    public static void main(String[] args) throws IOException {
        String omdbApiKey = System.getenv("OMDB_KEY");
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://www.omdbapi.com").addConverterFactory(GsonConverterFactory.create()).build();

        OMDBEndpoints omdbService = retrofit.create(OMDBEndpoints.class);

        Call<OMDBSearchResult> omdbSearchResult = omdbService.getMovieListFilteredByTitle("Age of Ultron", omdbApiKey);
        OMDBSearchResult result = omdbSearchResult.execute().body();
        System.out.println(result);
        System.out.println("Doing something 1...2...3...");
        System.out.println("Doing something 1...2...3...");
        System.out.println("Doing something 1...2...3...");
    }
}


