package manual;

import java.util.concurrent.*;

public class AsyncWithCompletableFuture {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ExecutorService executorService = Executors.newFixedThreadPool(3);

        CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(new SupplierFactorialTask(10), executorService);
        completableFuture.thenAccept(result -> {
            System.out.println("The result is " + result.toString());
        });
        System.out.println("Supposed to be another process");
        System.out.println("Supposed to be another tasks");
        System.out.println("Supposed to be another execution");

        //Other simulation, applying a method that supply callback and expecting a return value
        CompletableFuture<Integer> anotherCompletableFuture = CompletableFuture.supplyAsync(new SupplierFactorialTask(15), executorService);

        CompletableFuture<Integer> addingSomeValueToResult = anotherCompletableFuture.thenApply(result -> {
            return Integer.valueOf(result + 10);
        });

        System.out.println("Supposed to be another process again");
        System.out.println("Supposed to be another tasks again");
        System.out.println("Supposed to be another execution again");

        Integer anotherCompleteableFutureResult = addingSomeValueToResult.get();
        System.out.println("Factorial 15 added by 10 is "+anotherCompleteableFutureResult);

        System.out.println("Is this supposed to be the last statement to be executed?");

        //Chaining 2 Asynchronous Tasks

        CompletableFuture<String> exampleOfAnonymousCallback = CompletableFuture.supplyAsync(() -> {
            String returnedValue = "";
            try {
                returnedValue = (anotherCompleteableFutureResult.intValue() % anotherCompletableFuture.get().intValue() > 0) ? "Yes":"No";
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            return returnedValue;
        });

        CompletableFuture<String> chainingResult = exampleOfAnonymousCallback.thenCompose((someString) -> {
            return CompletableFuture.supplyAsync(() -> {return "Is the modulo grather than 0? " + someString;});
        });

        System.out.println(chainingResult.get());
    }
}
