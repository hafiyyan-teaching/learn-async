package manual;

import java.util.concurrent.*;

public class AsyncAndWaitSimulation {
    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        FactorialTask factorialTask = new FactorialTask(10);

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<Integer> result = executorService.submit(factorialTask);

        while(!result.isDone()){
            System.out.println("Waiting....");
        }
        Integer actualValue = result.get();
        System.out.println("The actual result is: "+actualValue.toString());

        FactorialTask anotherFactorialTask = new FactorialTask(15);

        try {
            Integer anotherValue = executorService.submit(anotherFactorialTask).get(10000, TimeUnit.MILLISECONDS);
            System.out.println("Another value is: "+anotherValue.toString());
        }
        catch (TimeoutException e){
            e.printStackTrace();
        }

        //Other functionality

    }
}
