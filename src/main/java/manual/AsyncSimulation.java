package manual;

import java.util.concurrent.*;

public class AsyncSimulation {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FactorialTask factorialTask = new FactorialTask(10);

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<Integer> result = executorService.submit(factorialTask);
        System.out.println(result.get());
        System.out.println("Supposed to be another process");
        System.out.println("Supposed to be another tasks");
        System.out.println("Supposed to be another execution");

    }
}
