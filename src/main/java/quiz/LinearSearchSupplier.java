package quiz;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class LinearSearchSupplier implements Supplier<Integer> {

    private List<Integer> searchableIntegerList;
    private int numberToBeSearched;

    public LinearSearchSupplier(int numberToBeSearched, List<Integer> sourceIntegerList){
        this.numberToBeSearched = numberToBeSearched;
        this.searchableIntegerList = new ArrayList<>(sourceIntegerList);
    }

    public void setNumberToBeSearched(int number){
        this.numberToBeSearched = number;
    }

    public void setSearchableIntegerList(List<Integer> sourceIntegerList){
        this.searchableIntegerList = new ArrayList<>(sourceIntegerList);
    }

    @Override
    public Integer get() {
        for(Integer element: searchableIntegerList)
        {
            if(element == this.numberToBeSearched)
                return searchableIntegerList.indexOf(element);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return -1;
    }
}
